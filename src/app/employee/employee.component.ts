import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  employees=[{name:"kaleem",age:32,role:"UI Developer"},
  {name:"rahul",age:30,role:"Software Developer"}
];
searchText;


  constructor() { }

  ngOnInit(): void {
  }

}
